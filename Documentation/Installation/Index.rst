.. include:: ../Includes.txt

.. _installation:

============
Installation
============

- Download the extension from TER or install via composer:

.. code::

   composer require susanne/hcaptcha

- Activate the extension in the TYPO3 Extension Manager
